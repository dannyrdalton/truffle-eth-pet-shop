pragma solidity 0.4.13;

contract Adoption {

  // state variables
  address[16] public adopters;

  event LogAdoptionBegin(address addr, uint petId);

  function adopt(uint petId) public returns (uint) {
    LogAdoptionBegin(msg.sender, petId);
    require(petId >= 0 || petId <= 15);

    adopters[petId] = msg.sender;

    return petId;
  }

  function getAdopters() public returns (address[16]) {
    return adopters;
  }
}